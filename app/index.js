const express = require('express');
const redis = require('redis');
const { Pool } = require('pg');

const app = express();

const client = redis.createClient({
  legacyMode: true,
  socket: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
  },
});

(async () => {
  try {
    await client.connect();
    console.log('Connected to Redis');
  } catch (err) {
    console.log('Failed to connect to Redis');
  }
})();

const pool = new Pool({
  user: 'postgres',
  host: 'postgres',
  database: 'mydb',
  password: '1qazxsw2',
});

(async () => {
  try {
    await pool.query(`
      CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        name VARCHAR(100)
      )
    `);
    console.log('Table created');
  } catch (err) {
    console.error('Failed to create table', err);
  }
})();

app.use(express.json());

app.post('/message', (req, res) => {
  const { key, message } = req.body;

  client.set(key, message);

  res.send('Message saved');
});

app.get('/message/:key', (req, res) => {
  const { key } = req.params;

  client.get(key, (err, value) => {
    if (err) {
      res.send('Error occurred');
      return;
    }

    res.send(value);
  });
});

app.post('/user', (req, res) => {
  const { name } = req.body;

  pool.query('INSERT INTO users (name) VALUES ($1)', [name], (err, results) => {
    if (err) {
      throw err;
    }

    res.send('User added');
  });
});

app.get('/users', (req, res) => {
  pool.query('SELECT * FROM users', (err, results) => {
    if (err) {
      throw err;
    }

    res.send(results.rows);
  });
});

app.listen(3000, () => {
  console.log(`Server is running on port 3000`);
});
